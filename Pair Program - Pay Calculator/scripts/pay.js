/*
Author: Ryan Hendrickson
Date: January 12th, 2018.
Filename: pay.js
Purpose: Takes the input from the html form and does the math for us.
*/

// Does the heavy lifting for our pay calculator when the user clicks submit
function calculate() {
	// Get our variables
	var fname = document.getElementById('fname').value;
	var hours = document.getElementById('hours').value;
	var payRate = document.getElementById('payRate').value;
	var holiday = document.getElementById('holiday').checked;

	// Declare our total variable and start doing math to figure out their pay
	var total = (hours*payRate);
	if(holiday) {
		total+=100;
	}

	// Display their pay
	document.getElementById('payOutput').innerHTML = "Hello " + fname + ", you earned $" + Number(total).toFixed(2);
}

// Resets the form and <h3>
function payOutputErase() {
	document.getElementById('payOutput').innerHTML = "";
}
