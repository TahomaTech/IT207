"use strict";

let validate = () => {
	// flag variable to track validity
	let isValid = true;

	// validate first name
	let fName = document.getElementById('custFName').value;
	let errfName = document.getElementById('error-fname');
	if (fName === "") {
		errfName.className = 'error';
		isValid = false;
	} else {
		errfName.className = 'hidden';
	}

	// validate last name
	let lName = document.getElementById('custLName').value;
	let errLName = document.getElementById('error-lname');
	if(lName === "") {
		errLName.className = 'error';
		isValid = false;
	} else {
		errLName.className = 'hidden';
	}

	// validate size
	let size = document.getElementById('size').value;
	let errSize = document.getElementById('error-size');
	if (size === 'none') {
		errSize.className = 'error';
		isValid =false;
	} else {
		errSize.className = 'hidden';
	}

	// validate terms
	let terms = document.getElementById('terms').checked;
	let errTerms = document.getElementById('error-terms');
	if (!terms) {
		errTerms.className = 'error';
		isValid = false;
	} else {
		errTerms.className = 'hidden';
	}

	// validate radios
	let methods = document.getElementsByName('custMethod');
	let errMethods = document.getElementById('error-radio');
	for (var i = 0; i < methods.length; i++) {
		if (methods[i].checked === true) {
			errMethods.className = 'hidden';
		}
	}

	// validate address | and it doesn't work that well
	let address = document.getElementById('custAdd').value;
	let errAdd = document.getElementById('error-address');
	let delivery = document.getElementById('delivery').checked;
	if (delivery) {
		if (address === "") {
			errAdd.className = 'error';
			isValid = false;
		} else {
			errAdd.className = 'hidden';
		}
	}
	// if isValid is false, it won't let the form submit
	return isValid;
}
